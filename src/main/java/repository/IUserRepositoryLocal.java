package repository;

import javax.ejb.Local;

@Local
public interface IUserRepositoryLocal extends IUserRepository {
}